
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var apiKey = "f1vsTBbfA9eMrYZdtpx5shhJQdWwHYAMu8QGFhsn"; //for openFDA
var ehrIdtable = [];
var ehrIdchosen = "";
var mapa;
var json;
var lastMarkIx;


window.addEventListener('load', function () {
 
  mapa = L.map('mapa_id').setView([46.15004, 14.88911], 8); 

  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  mapa.addLayer(layer);
  
  
  //dodajMarker(46.05004, 14.46931,"podatek 1 in podatek 2","tip");
  
  pridobiPodatke(function (jsonRezultat) {
    izrisRezultatov(jsonRezultat);
  });
  
  
  var popup = L.popup();
  function obKlikuNaMapo(e) {
    var latlng = e.latlng;

    if(lastMarkIx) {
      dodajMarker(json.features[lastMarkIx].geometry.coordinates, 
                  json.features[lastMarkIx].properties.name, 
                  json.features[lastMarkIx].geometry.type, "blue");
    }
    var ix = najdiNajblizji(latlng);
    dodajMarker(json.features[ix].geometry.coordinates, json.features[ix].properties.name, json.features[ix].geometry.type, "green");
    lastMarkIx = ix;
    
  }

  mapa.on('click', obKlikuNaMapo);
  
  
  
  /*napolni seznam bolezni*/
  
  
});





//++++++++++++++++++++++++++bolnica++++++++++++++++++++++++++++++++++++++++++++++++++++
function dodajMarker(latlng,opis,tip,barva) {
  
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-' + barva + '.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  
  var marker;
  if(tip == "Point") {
    var coordinateMark = L.latLng(latlng[1], latlng[0]);
    marker = L.marker(coordinateMark, {icon: ikona});
    marker.bindPopup("<div> " + opis + "</div>");
    marker.addTo(mapa);
  } else {
    //poligon
    for(var i = 0; i < latlng.length; i++){ //zanka čez vsako stavbo (vsakim polygonom)
      var poligon = [];
      for(var k = 0; k < latlng[i].length; k++) {//zanka čez vsako točko polygona
        //zamenjaj lat in lng
        poligon.push( [ latlng[i][k][1],latlng[i][k][0] ] );
        
      }
      
      if(poligon[0][0]) {
        marker = L.polygon(poligon, {color: barva});
        marker.bindPopup("<div> " + opis + "</div>");
        marker.addTo(mapa);
      } 
    }
    
  }
}

function izrisRezultatov(jsonRezultat) {
  var bolnisnice = jsonRezultat.features;

  for (var i = 0; i < bolnisnice.length-2; i++) {
    //var jeObmocje = typeof(bolnisnice[i].geometry.coordinates[0]) == "object";
    
    var opis = bolnisnice[i].properties.name + "<br>";
        opis += (bolnisnice[i].properties["addr:street"]? (bolnisnice[i].properties["addr:street"] + " " + 
                bolnisnice[i].properties["addr:housenumber"]): "") + ", " + 
                (bolnisnice[i].properties["addr:city"]?bolnisnice[i].properties["addr:city"]:"");
        
    

    var coord = bolnisnice[i].geometry.coordinates;
    
    dodajMarker(coord, opis, bolnisnice[i].geometry.type, "blue");
  }
}

function pridobiPodatke(callback) {
  //if (typeof(vrstaInteresneTocke) != "string") return;

  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
  xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        json = JSON.parse(xobj.responseText);
        
       //json["features"]
       
        callback(json);
    }
  };
  xobj.send(null);
}

function najdiNajblizji(latlng){
  var bolnisnice = json.features;

  var ixMin = 0; 
  var minDist = 10000;
  
  for (var i = 0; i < bolnisnice.length-2; i++) {
    var arr = bolnisnice[i].geometry.coordinates;
    
    var razdalja;
    if(Array.isArray(arr[0])) {
      razdalja = distance(latlng.lat, latlng.lng, arr[0][0][1], arr[0][0][0], 'K');
    } else {
      razdalja = distance(latlng.lat, latlng.lng, arr[1],arr[0], 'K');
    }
    
    
    if(razdalja<minDist) {
      ixMin = i;
      minDist = razdalja;
    }
  }
  
  return ixMin;
}









//++++++++++++++++++++++++++glavna funkcionalnost++++++++++++++++++++++++++++++++++++++++++++++++++++
/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke() {
  
  var tabelaPacientov = [{ime:"Jani", priimek:"Razni", datum:"1983-09-29T13:10:00.000Z"},
                      {ime:"Jure", priimek:"Tuta", datum:"1988-01-11T19:10:00.000Z"},
                      {ime:"Andrej", priimek:"Rutar", datum:"1999-03-18T13:10:00.000Z"}];

  for(var key in tabelaPacientov) {
    generirajEnega(tabelaPacientov[key].ime, tabelaPacientov[key].priimek, tabelaPacientov[key].datum);
  }
  
  $("#errMsg").html("Uspešno ustvarjeni 3 zapisi.").css({"background-color":"#87ff6c", "visibility":"visible"});
}

function generirajEnega(ime, priimek, datumRojstva){
  $.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: { "Authorization": getAuthorization() },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {"ehrId": ehrId}
        };
        
        $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
             
              var optionHtmlString = "<option value='" + ehrId + "'>" +ime + " "+ priimek +"</option>";
              $("#created").html($("#created").html() + optionHtmlString);
              ehrIdtable.push(ehrId);
              
              $("#errMsg").html("Uspešno ustvarjen: <i> " + ehrId + "</i>").css({"background-color":"#87ff6c", "visibility":"visible"});
            }
          },
          error: function(err) {
            $("#errMsg").html("Napaka pri kreiranju!").css({"background-color":"#fe6f6f", "visibility":"visible"});
          }
        });
      }
  });
}



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function createPacient(){
  
  if(document.getElementById("ehrid").value != ""){
    
   //izberi obstoječega pacienta glede na ehrID
   ehrIdchosen = document.getElementById("ehrid").value;
   $("#errMsg").html("Izbran: <i> " + ehrIdchosen + "</i>").css({"background-color":"#99b0ff", "visibility":"visible"});
   
   
  } else if(document.getElementById("created").value != ""){
    
    //izberi generiranega pacienta
    ehrIdchosen = document.getElementById("created").value;
    $("#errMsg").html("Izbran: <i> " + ehrIdchosen + "</i>").css({"background-color":"#99b0ff", "visibility":"visible"});
    
    
  } else {
    
    //ustvari nov zapis EHR za pacienta
    var ime = document.querySelector("input[name=firstname]").value.trim(); 
    var priimek = document.querySelector("input[name=lastname]").value.trim(); 
    var date = document.querySelector("input[name=dateofbirth]").value;
    
    document.querySelector("input[name=firstname]").value = ""; 
    document.querySelector("input[name=lastname]").value = ""; 
    document.querySelector("input[name=dateofbirth]").value = "";
    
    if(ime != "" & priimek != "" & date != "" ){
      //2019-05-14   --->   1983-09-29T13:10:00.000Z
      date = date + "T00:00:00.000Z";
      generirajEnega(ime,priimek,date);
      ehrIdchosen = ehrIdtable[ehrIdtable.length-1];
    }
    
  }
}


function recomendMedication(){
  var numOfRequests = 0;
  var krvniTlak = document.querySelector("input[name=bloodpressure]").value.trim(); 
  var temp = document.querySelector("input[name=temperature]").value.trim(); 
  var symptoms = document.querySelector("input[name=symptoms]").value.trim();
  var diseases = document.getElementById("diseases").value;
  
  diseases = diseases.replace(" ", "+AND+").replace("-", "+AND+");
  var urlFull = "https://api.fda.gov/drug/label.json?api_key=" + apiKey + "&search=purpose:(" + diseases + ")&limit=99";
  
  const Http = new XMLHttpRequest();
  Http.open("GET", urlFull);
  Http.send();
  Http.onreadystatechange=(e)=>{
    if(numOfRequests == 1){
      //var jsonArr = Http.response;
      var jsonObj = $.parseJSON(Http.response);
      document.getElementById("meds").innerHTML = "";
      
      for(var i = 0; i<jsonObj.results.length; i++) {

        if(jsonObj.results[i].openfda.generic_name){
         
          var node = document.createElement("option");
          node.value = jsonObj.results[i].id;
          
          //max char lenght == 66
          var genName = jsonObj.results[i].openfda.generic_name;
          var brName = jsonObj.results[i].openfda.brand_name;
          node.textContent = genName + " - " + brName;
          
          document.getElementById("meds").appendChild(node);
          document.getElementById("medications").style.visibility = "visible";
          
        }
      }
      
    }
    numOfRequests++;
  };
}


function moreInfo() {
  var medSelect = document.getElementById("meds");
  var medicationID = medSelect.options[ medSelect.selectedIndex ].value;
  
  var urlFull = "https://api.fda.gov/drug/label.json?api_key=" + apiKey + "&search=id:"+ medicationID ;
  var numOfRequests = 0;
  const Http = new XMLHttpRequest();
  Http.open("GET", urlFull);
  Http.send();
  Http.onreadystatechange=(e)=>{
    if(numOfRequests == 1){
      if(Http.response) {
        
        var jsonObj = $.parseJSON(Http.response);
      
        var string = jsonObj.results[0].openfda.generic_name + " (" + jsonObj.results[0].openfda.brand_name + 
                    ")\n---------------------------------------\n" + 
                    "Manufacturer: " + jsonObj.results[0].openfda.manufacturer_name + 
                    "\n" + jsonObj.results[0].purpose + 
                    "\nUsage: " + jsonObj.results[0].indications_and_usage + 
                    "\nIngredients: " + jsonObj.results[0].inactive_ingredient;
        
        alert(string); 
        
      }
    }
    numOfRequests++;
  };
}

function newMedicalReport() {
  var ehrId = ehrIdchosen;
	var datumInUra = "2014-05-26T10:06:08.356+02:00";
	var krvniTlak = $("input[name=bloodpressure]").val();
	var telesnaTemperatura = $("input[name=temperature]").val();
	var simptomi = $("input[name=symptoms]").val();
	var merilec = "zdravnik1";

  if(ehrId &&  datumInUra && krvniTlak && telesnaTemperatura && simptomi){
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    //"vital_signs/body_temperature/any_event/symptoms|label": simptomi,
		    //"vital_signs/body_temperature/any_event/symptoms|value": "at0.64",
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": krvniTlak
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        var medSelect = document.getElementById("meds");
        var medicationID = medSelect.options[ medSelect.selectedIndex ].value;
        
        var urlFull = "https://api.fda.gov/drug/label.json?api_key=" + apiKey + "&search=id:"+ medicationID ;
        var numOfRequests = 0;
        const Http = new XMLHttpRequest();
        Http.open("GET", urlFull);
        Http.send();
        Http.onreadystatechange=(e)=>{
          if(numOfRequests == 1){
            if(Http.response) {
              
              var jsonObj = $.parseJSON(Http.response);
                          
              var idzdravila = medicationID;
              var imeZdravila = jsonObj.results[0].openfda.generic_name + " (" + jsonObj.results[0].openfda.brand_name + ")";
              
              var podatki = {"ctx/language": "en",
          		    "ctx/territory": "SI",
          		    "ctx/time": datumInUra,
          		    "medications/medication_instruction/order/medicine": idzdravila + " " + imeZdravila,
          		    "medications/medication_instruction/order/directions": jsonObj.results[0].dosage_and_administration,
          		    "medications/medication_instruction/order/clinical_indication": jsonObj.results[0].purpose
          		};
          		var parametriZahteve = {
          		    ehrId: ehrId,
          		    templateId: 'Medications',
          		    format: 'FLAT',
          		    committer: merilec
          		};
          		
          		console.log(podatki);
          		$.ajax({
                url: baseUrl + "/composition?" + $.param(parametriZahteve),
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(podatki),
                headers: {
                  "Authorization": getAuthorization()
                },
                success: function (res) {
                  alert("Poizvedba uspešna!");
                  clear();
                },
                error: function(err) {
                	alert("Napaka v poizvedbi! \nProsimo izpolnite vse podatke.");
                }
          		});
              
            }
          }
          numOfRequests++;
        };
      },
      error: function(err) {
      	alert("Napaka v poizvedbi! \nProsimo izpolnite vse podatke.");
      }
		});
	}
}
function clear() {
  $("#errMsg").css("visibility","hidden");
  $("#created").val("");
  $("input[name=bloodpressure]").val("");
	$("input[name=temperature]").val("");
	$("input[name=symptoms]").val("");
  $("#diseases").val("");
  $("#medications").css("visibility","hidden");
  $("#ehrid").val("");
}