window.addEventListener('load', function () {
  makeThermo();
  document.querySelector("input[name=temperature]").addEventListener("keyup",makeThermo);
});

function makeThermo() {
  document.getElementById("demo").innerHTML = "";
  
  var rectWidth = 15,
  	rectHeight = 80,
      rectX= 10,
      rectY = 10; 
      
  var temp = Number(document.querySelector("input[name=temperature]").value);
  var temperature = (temp>=0 && temp<=50) ? temp : 0;
  var	maxTemp = 50;
  
  var demo = d3.select('#demo');
  var svg = demo.append('svg');
  
  for(var i = 0; i<=maxTemp/10; i++) {
  svg.append('line')
  	.attr('x1',rectX-3)
      .attr('y1',rectY+(rectHeight - i*(rectHeight/(maxTemp/10))))
      .attr('x2',rectX+rectWidth)
      .attr('y2',rectY+(rectHeight - i*(rectHeight/(maxTemp/10))))
  	.attr('stroke', 'black')
      .attr('stroke-width', '1px');
  }
  
  svg.append("circle")
    .attr("r", rectWidth/2)
    .attr("cx", rectX + rectWidth/2)
    .attr("cy", rectY)
    .style("fill", "white")
    .style("stroke", "black")
    .style("stroke-width", "1px");
  
  svg.append('rect')
     .attr('x', rectX)
     .attr('y', rectY)
     .attr('width', rectWidth)
     .attr('height', rectHeight)
     .attr('fill', 'white')
     .attr('stroke', 'black')
     .attr('stroke-width', '1px');
     
  svg.append('line')
  	.attr('x1',rectX)
      .attr('y1',rectY)
      .attr('x2',rectX+rectWidth)
      .attr('y2',rectY)
  	.attr('stroke', 'white')
      .attr('stroke-width', '2px');
     
  svg.append("circle")
    .attr("r", rectWidth)
    .attr("cx", rectX + rectWidth/2)
    .attr("cy", rectY+rectHeight+rectWidth -10)
    .style("fill", "white")
    .style("stroke", "black")
    .style("stroke-width", "1px");
   
  var innerW = rectWidth-6;
  var innerH = rectHeight*temperature/maxTemp;
  
  svg.append("rect")
  	.attr('x', rectX + 3)
    	.attr('y', rectY + rectHeight-innerH)
     	.attr('width', innerW)
     	.attr('height', innerH)
     	.attr('fill', 'red');
      
  svg.append("circle")
    .attr("r", rectWidth-2)
    .attr("cx", rectX + rectWidth/2)
    .attr("cy", rectY+rectHeight+rectWidth -10)
    .style("fill", "red");
  
  svg.style("width","100%");
}